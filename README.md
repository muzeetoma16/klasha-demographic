All the information from external endpoints are gotten asynchronously via separate threads.
This is to keep the main thread free of external workload overhead to ensure high performance and avoid thread blocking.

I made sure to update the exchange rates on every fresh application start-up,
it is also possible to create and endpoint that would receive a Multipart file 
and update the rates at will. 
Another possibility is to use a cronJob to update the exchange rates periodically.

I have included tests and a swagger documentation

N/B: Most of the Endpoints have been changed from POST requests to GET and their endpoints have been modified.

I could not obtain the cities in a state because the endpoint on the free API is faulty.
https://countriesnow.space/api/v0.1/countries/state/cities/q?country=Nigeria&state=Lagos
