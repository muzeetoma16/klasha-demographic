package com.klasha.demographicapp.controller;

import com.klasha.demographicapp.dtos.request.CurrencyConversionRequest;
import com.klasha.demographicapp.service.CurrencyConversionService;
import com.klasha.demographicapp.service.DemographicService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class DemographicController {

  private final DemographicService demographicService;
  private final CurrencyConversionService currencyConversionService;

  //Get the most populous cities from a group of modifiable countries defined

  @GetMapping("/population")
  public ResponseEntity<?> getPopulatedCities(@RequestParam("N") int N) {
    return ResponseEntity.ok(demographicService.getMostPopulatedCities(N));
  }

  //Get vital info about like population, location etc a country
  @GetMapping("/info")
  public ResponseEntity<?> getPopulatedCities(@RequestParam("country") String country) {
    return ResponseEntity.ok(demographicService.getCountryInfo(country));
  }

  //Get All States with their cities in a country
  @GetMapping("/states")
  public ResponseEntity<?> getStatesInCountry(@RequestParam("country") String country) {
    return ResponseEntity.ok(demographicService.getCitiesInState(country));
  }

  //Currency conversion
  @PostMapping("/currency")
  public ResponseEntity<?> convertCurrency(@RequestBody @Valid CurrencyConversionRequest currencyConversionRequest) {
    return ResponseEntity.ok(currencyConversionService.convertCurrency(currencyConversionRequest));
  }
}
