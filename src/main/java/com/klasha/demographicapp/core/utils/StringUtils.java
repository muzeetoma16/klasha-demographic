package com.klasha.demographicapp.core.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

  public static String removeUrlEncodedSpaces(String value){
    return value.replace("%20"," ");
  }
  public static int extractInt(String value) {
    List<Integer> values = new ArrayList<>();
    Pattern pattern = Pattern.compile("\\d+");
    Matcher matcher = pattern.matcher(value);

    while (matcher.find()) {
      int extractedInt = Integer.parseInt(matcher.group());
      values.add(extractedInt);
    }

    return !isNullOrEmpty(values) ? values.get(0) : 0;
  }

  public static long extractLong(String value) {
    List<Long> values = new ArrayList<>();
    Pattern pattern = Pattern.compile("\\d+");
    Matcher matcher = pattern.matcher(value);

    while (matcher.find()) {
      long extractedInt = Long.parseLong(matcher.group());
      values.add(extractedInt);
    }

    return !isNullOrEmpty(values) ? values.get(0) : 0;
  }

  public static boolean isNullOrEmpty(Set<?> object) {
    return object == null || object.isEmpty();
  }

  public static boolean isNullOrEmpty(List<?> object) {
    return object == null || object.isEmpty();
  }

  public static boolean isNullOrEmpty(Map<?, ?> object) {
    return object == null || object.isEmpty();
  }

  public static boolean isEmptyOrBlank(String object) {
    return object == null || object.isEmpty() || object.isBlank();
  }
}
