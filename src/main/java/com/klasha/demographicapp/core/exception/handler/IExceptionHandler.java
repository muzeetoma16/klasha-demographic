package com.klasha.demographicapp.core.exception.handler;

import com.klasha.demographicapp.core.exception.BadRequestException;
import com.klasha.demographicapp.core.exception.ConflictException;
import com.klasha.demographicapp.core.exception.ForbiddenException;
import com.klasha.demographicapp.core.exception.NotFoundException;
import com.klasha.demographicapp.core.exception.response.ErrorResponse;
import com.klasha.demographicapp.core.exception.response.ResponseDto;
import java.rmi.ServerError;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpServerErrorException;

@RestControllerAdvice
public class IExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseDto<?> handleMethodArgumentException(
			MethodArgumentNotValidException exception) {
		ResponseDto<?> serviceResponse = new ResponseDto<>();
		List<ErrorResponse> errors = new ArrayList<>();
		exception.getBindingResult().getFieldErrors().forEach(error -> {
			ErrorResponse errorResponse = new ErrorResponse(error.getField(),
					error.getDefaultMessage());
			errors.add(errorResponse);
		});
		serviceResponse.setStatus("FAILED");
		serviceResponse.setErrors(errors);
		serviceResponse.setStatusCode(HttpStatus.BAD_REQUEST.value());
		return serviceResponse;
	}

	@ExceptionHandler(value = { NullPointerException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public ResponseDto<?> nullPointerErrorHandler(NotFoundException ex) {
		ResponseDto<?> responseDto = new ResponseDto<>();
		responseDto.setStatus("FAILED");
		responseDto.setStatusCode(HttpStatus.NOT_FOUND.value());
		responseDto.setErrors(Collections.singletonList(new ErrorResponse("",
				"Something went wrong! " + "The request could not be completed!")));
		return responseDto;
	}

	@ExceptionHandler(value = { NotFoundException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public ResponseDto<?> misMatchErrorHandler(NotFoundException ex) {
		ResponseDto<?> responseDto = new ResponseDto<>();
		responseDto.setStatus("FAILED");
		responseDto.setStatusCode(HttpStatus.NOT_FOUND.value());
		responseDto.setErrors(
				Collections.singletonList(new ErrorResponse("", ex.getMessage())));
		return responseDto;
	}

	@ExceptionHandler(value = { ForbiddenException.class })
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	public ResponseDto<?> forbiddenErrorHandler(ForbiddenException ex) {
		ResponseDto<?> responseDto = new ResponseDto<>();
		responseDto.setStatus("FAILED");
		responseDto.setStatusCode(HttpStatus.UNAUTHORIZED.value());
		responseDto.setErrors(
				Collections.singletonList(new ErrorResponse("", ex.getMessage())));
		return responseDto;
	}

	@ExceptionHandler(value = { ServerError.class })
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseDto<?> internalServerErrorHandler(
			HttpServerErrorException.InternalServerError ex) {
		ResponseDto<?> responseDto = new ResponseDto<>();
		responseDto.setStatus("FAILED");
		responseDto.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		responseDto.setErrors(
				Collections.singletonList(new ErrorResponse("", ex.getMessage())));
		return responseDto;
	}


	@ExceptionHandler(value = { ConflictException.class })
	@ResponseStatus(value = HttpStatus.CONFLICT)
	public ResponseDto<?> ConflictException(ConflictException ex) {
		ResponseDto<?> responseDto = new ResponseDto<>();
		responseDto.setStatus("FAILED");
		responseDto.setStatusCode(HttpStatus.CONFLICT.value());
		responseDto.setErrors(
				Collections.singletonList(new ErrorResponse("", ex.getMessage())));
		return responseDto;
	}
	@ExceptionHandler(value = { IllegalArgumentException.class })
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ResponseDto<?> illegalArgumentExceptionHandler(IllegalArgumentException ex) {
		ResponseDto<?> responseDto = new ResponseDto<>();
		responseDto.setStatus("FAILED");
		responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());
		responseDto.setErrors(
				Collections.singletonList(new ErrorResponse("", ex.getMessage())));
		return responseDto;
	}

	@ExceptionHandler(value = { BadRequestException.class })
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ResponseDto<?> badRequestExceptionHandler(BadRequestException ex) {
		ResponseDto<?> responseDto = new ResponseDto<>();
		responseDto.setStatus("FAILED");
		responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());
		responseDto.setErrors(
				Collections.singletonList(new ErrorResponse("", ex.getMessage())));
		return responseDto;
	}


}
