package com.klasha.demographicapp.core.exception.response;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponseDto<T> {

	private T results;

	private int statusCode;

	private String status;

	private List<ErrorResponse> errors;

}
