package com.klasha.demographicapp.core.config;

import static com.klasha.demographicapp.core.currency.CurrencyExchangeRates.exchangeRates;

import com.klasha.demographicapp.core.currency.Currency;
import com.klasha.demographicapp.core.currency.ExchangeRate;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AppStartup implements ApplicationListener<ApplicationReadyEvent> {

  @Override
  public void onApplicationEvent(ApplicationReadyEvent event) {
    try {
      consumeExchangeRates();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void consumeExchangeRates() throws IOException {
    Resource resource = new ClassPathResource("exchange_rate.csv");
    try(BufferedReader bufferedReader= new BufferedReader(
        new FileReader(resource.getFile().getAbsoluteFile())
    )){
      String line;
      while ((line = bufferedReader.readLine())!=null){
        String[] values = line.split(",");

        try {
          Currency sourceCurrency = Currency.valueOf(values[0]);
          Currency targetCurrency = Currency.valueOf(values[1]);
          BigDecimal sourceRate = BigDecimal.valueOf(Double.parseDouble(values[2]));

          BigDecimal targetRate = values.length > 3 ?
              BigDecimal.valueOf(Double.parseDouble(values[3]))
              : BigDecimal.ONE;

         exchangeRates.add(new ExchangeRate(sourceCurrency,targetCurrency,sourceRate,targetRate));
        }catch (Exception e){
         log.error(""+e);
        }
      }
    }

    exchangeRates.forEach(System.out::println);
  }

}
