package com.klasha.demographicapp.core.config;

import com.klasha.demographicapp.core.exception.BadRequestException;
import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


@Configuration
@EnableAsync
public class AsyncConfiguration extends AsyncConfigurerSupport {

	private static final String TASK_EXECUTOR_NAME_PREFIX_SERVICE = "serviceTaskExecutor-";
	public static final String TASK_EXECUTOR_SERVICE = "serviceTaskExecutor";

	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return new AsyncExceptionHandler();
	}

	@Bean(name = TASK_EXECUTOR_SERVICE)
	public Executor getServiceAsyncExecutor() {
		return newTaskExecutor();
	}


	private Executor newTaskExecutor() {
		final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		configureExecutor(executor);
		return executor;
	}

	private void configureExecutor(final ThreadPoolTaskExecutor executor) {
		executor.setThreadNamePrefix(
				AsyncConfiguration.TASK_EXECUTOR_NAME_PREFIX_SERVICE);
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(20);
		executor.setQueueCapacity(50);
		executor.setAllowCoreThreadTimeOut(true);
		executor.setKeepAliveSeconds(60);
		executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
		executor.initialize();
	}

	static class AsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

		@Override
		public void handleUncaughtException(Throwable ex, Method method,
				Object... params) {
			System.out.println("Unexpected asynchronous exception at : "
					+ method.getDeclaringClass().getName() + "." + method.getName() + ex);

			throw new BadRequestException(ex.getMessage());
		}

	}

}
