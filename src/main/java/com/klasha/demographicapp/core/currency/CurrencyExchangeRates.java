package com.klasha.demographicapp.core.currency;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CurrencyExchangeRates {
  public static final Set<ExchangeRate> exchangeRates = new HashSet<>();
  public static BigDecimal currencyExchange(Currency sourceCurrency, Currency targetCurrency,
      BigDecimal sourceAmount){

     ExchangeRate exchangeRate= exchangeRates.stream()
        .filter(exr ->
            (exr.getSourceCurrency().equals(sourceCurrency) &&
            exr.getTargetCurrency().equals(targetCurrency)) ||

                (exr.getSourceCurrency().equals(targetCurrency) &&
                    exr.getTargetCurrency().equals(sourceCurrency))
        )
        .findFirst()
        .orElse(null);

     BigDecimal exrRate = BigDecimal.ONE;
     BigDecimal exrRateTo = BigDecimal.ONE;

     if(exchangeRate!=null) {
       if (sourceCurrency == exchangeRate.getSourceCurrency()) {
           exrRate=exchangeRate.getSourceRate();
       }else {
         exrRate=exchangeRate.getTargetRate();
       }

       if(targetCurrency==exchangeRate.getTargetCurrency()){
         exrRateTo=exchangeRate.getTargetRate();
       }else {
         exrRateTo = exchangeRate.getSourceRate();
       }
     }

    log.info(":: ExRate= "+exrRate+" ExRateTo= "+exrRateTo
        +" Source Curr= "+sourceCurrency+ " Target Curr "+targetCurrency+ " ::");

     double convertedValue = (sourceAmount.doubleValue() / exrRateTo.doubleValue()) * exrRate.doubleValue();

    return BigDecimal.valueOf(convertedValue).setScale(2,RoundingMode.HALF_UP);
  }

}
