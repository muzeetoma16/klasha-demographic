package com.klasha.demographicapp.core.currency;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExchangeRate {
  Currency sourceCurrency;
  Currency targetCurrency;
  BigDecimal sourceRate;
  BigDecimal targetRate;
}
