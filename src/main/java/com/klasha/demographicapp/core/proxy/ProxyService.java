package com.klasha.demographicapp.core.proxy;

import static com.klasha.demographicapp.core.config.AsyncConfiguration.TASK_EXECUTOR_SERVICE;

import java.util.concurrent.CompletableFuture;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class ProxyService {

  public static long TIME_OUT = 70_000L;
  public static long SHORT_TIME_OUT = 40_000L;

  @Autowired
  private RestTemplate restTemplate;


  @Async(value = TASK_EXECUTOR_SERVICE)
  public <R,P> CompletableFuture<P> execute(ProxyRequest proxyRequest,
                                             Class<P> responseClass,
                                             Class<R> requestClass) {

    try {

      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_JSON);

      return CompletableFuture
          .supplyAsync(() -> {
            ResponseEntity<P> responseEntity = null;
            if (proxyRequest.getHttpMethod() == HttpMethod.POST) {

              HttpEntity<R> httpEntity = new HttpEntity<>(
                  requestClass.cast(proxyRequest.getRequest()),
                  headers
              );

              responseEntity = restTemplate.exchange(
                  proxyRequest.getUrl(),
                  HttpMethod.POST,
                  httpEntity,
                  responseClass
              );
            } else if (proxyRequest.getHttpMethod() == HttpMethod.GET) {
              try {
                responseEntity = restTemplate.getForEntity(
                    proxyRequest.getUrl(),
                    responseClass
                );
              }catch (HttpClientErrorException e){
                log.error("::: "+e.getMessage()+" :::");
                return null;
              }
            }
            return responseEntity != null ? responseEntity.getBody() : null;
          });
    } catch (HttpClientErrorException e) {
      log.error("::: "+e.getMessage()+" :::");
      return null;
    }
  }

}
