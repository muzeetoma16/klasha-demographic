package com.klasha.demographicapp.core.proxy;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpMethod;

@AllArgsConstructor
@Builder
@Data
public class ProxyRequest {
  private String url;
  private HttpMethod httpMethod;
  private Object request;
  private Map<String, ?> values;
}
