package com.klasha.demographicapp.dtos.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class MostPopulatedData {
  String city;
  String population;
}
