package com.klasha.demographicapp.dtos.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class CurrencyConversionData {
  String countryCurrency;
  String convertedCurrency;
}
