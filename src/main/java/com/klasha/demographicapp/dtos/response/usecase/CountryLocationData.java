package com.klasha.demographicapp.dtos.response.usecase;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.klasha.demographicapp.dtos.response.DataResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
public class CountryLocationData extends DataResponse {
  LocationData data;

  @Data
  public static class LocationData{
    String name;
    @JsonProperty("long")
    double longitude;
    @JsonProperty("lat")
    double latitude;
  }

}
