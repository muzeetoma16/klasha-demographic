package com.klasha.demographicapp.dtos.response.usecase;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.klasha.demographicapp.dtos.response.DataResponse;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CountryStateData extends DataResponse {
   StateData data;

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
   public static class StateData{
     List<State> states;
   }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  public static class State{
    String name;
    @JsonProperty("state_code")
    String stateCode;
  }

}
