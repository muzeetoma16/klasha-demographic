package com.klasha.demographicapp.dtos.response;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class StateData {
  String name;
  List<CityData> cities;

  @AllArgsConstructor
  @Data
  public static class CityData{
    Long name;
  }
}
