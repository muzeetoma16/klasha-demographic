package com.klasha.demographicapp.dtos.response;

import com.klasha.demographicapp.dtos.response.usecase.CountryLocationData.LocationData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class CountryData{
  String capital;
  String iso2;
  String iso3;
  String population;
  String currency;
  LocationData location;

  @AllArgsConstructor
  @Data
  public static class LocationData{
    double longitude;
    double latitude;
  }
}
