package com.klasha.demographicapp.dtos.response.usecase;

import static com.klasha.demographicapp.core.utils.StringUtils.extractInt;
import static com.klasha.demographicapp.core.utils.StringUtils.extractLong;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.klasha.demographicapp.dtos.response.DataResponse;
import java.util.Comparator;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CountryPopulationData extends DataResponse {
   List<PopulationData> data;

   @Data
   @AllArgsConstructor
   @NoArgsConstructor
   public static class PopulationData implements Comparable<PopulationData>{
      String city;
      String country;
      List<PopulationCount> populationCounts;

      //get the most up-to-date-count
      public PopulationCount getLatestPopulationCount(){
        return this.getPopulationCounts().stream()
             .max(Comparator.comparing(PopulationCount::getYear))
             .orElse(null);
      }

      @JsonIgnore
      @Override
      public int compareTo(PopulationData other) {
         PopulationCount thisHighest = this.getLatestPopulationCount();
         PopulationCount otherHighest = other.getLatestPopulationCount();

         //to handle null cases appropriately
         if (thisHighest == null && otherHighest == null) {
            return 0;
         } else if (thisHighest == null) {
            return -1;
         } else if (otherHighest == null) {
            return 1;
         } else {
            return Long.compare(thisHighest.getComparator(), otherHighest.getComparator());
         }
      }

      @Data
      public static class PopulationCount{
         String year;
         String value;
         String sex;
         @JsonProperty("reliabilty")
         String reliability;

         @JsonIgnore
         long getComparator(){
            return extractLong(value);
         }
      }
   }
}

