package com.klasha.demographicapp.dtos.response.usecase;

import com.klasha.demographicapp.dtos.response.DataResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
public class CountryCurrencyData extends DataResponse {
  CurrencyData data;
  @Data
  public static class CurrencyData {
    String name;
    String currency;
  }

}
