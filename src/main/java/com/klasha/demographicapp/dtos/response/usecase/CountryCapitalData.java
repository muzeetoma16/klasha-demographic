package com.klasha.demographicapp.dtos.response.usecase;

import com.klasha.demographicapp.dtos.response.DataResponse;
import lombok.Data;


@Data
public class CountryCapitalData extends DataResponse {
  CapitalData data;
  @Data
  public static class CapitalData {
    String name;
    String capital;
    String iso2;
    String iso3;
  }
}
