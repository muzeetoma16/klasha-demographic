package com.klasha.demographicapp.dtos.response.usecase;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.klasha.demographicapp.dtos.response.DataResponse;
import java.util.Comparator;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CityPopulationData extends DataResponse {
  PopulationData data;

  @Data
  @NoArgsConstructor
  public static class PopulationData{
    String city;
    String country;
    List<PopulationCount> populationCounts;

    public PopulationCount getLatestPopulationCount(){
      return this.getPopulationCounts().stream()
          .max(Comparator.comparing(PopulationCount::getYear))
          .orElse(null);
    }

    @Data
    @NoArgsConstructor
    public static class PopulationCount{
      String year;
      String value;
      String sex;
      @JsonProperty("reliabilty")
      String reliability;
    }
  }

}

