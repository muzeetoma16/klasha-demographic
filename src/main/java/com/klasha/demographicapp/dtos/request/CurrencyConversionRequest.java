package com.klasha.demographicapp.dtos.request;

import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CurrencyConversionRequest {

  @NotBlank(message = "Country is required")
  String country;

  @NotNull(message = "An amount must be provided")
  BigDecimal amount;

  @NotBlank(message = "A target currency is required")
  String targetCurrency;
}
