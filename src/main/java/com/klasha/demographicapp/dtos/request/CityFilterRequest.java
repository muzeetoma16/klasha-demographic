package com.klasha.demographicapp.dtos.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CityFilterRequest {
   int limit;
   String order;
   String orderBy;
   String country;
}
