package com.klasha.demographicapp.service.impl;

import static com.klasha.demographicapp.core.proxy.ProxyService.TIME_OUT;
import static com.klasha.demographicapp.core.utils.StringUtils.isNullOrEmpty;

import com.klasha.demographicapp.dtos.response.CountryData;
import com.klasha.demographicapp.dtos.response.CountryData.LocationData;
import com.klasha.demographicapp.dtos.response.MostPopulatedData;
import com.klasha.demographicapp.dtos.response.StateData;
import com.klasha.demographicapp.dtos.response.usecase.CityPopulationData;
import com.klasha.demographicapp.dtos.response.usecase.CityPopulationData.PopulationData.PopulationCount;
import com.klasha.demographicapp.dtos.response.usecase.CountryCapitalData;
import com.klasha.demographicapp.dtos.response.usecase.CountryCurrencyData;
import com.klasha.demographicapp.dtos.response.usecase.CountryLocationData;
import com.klasha.demographicapp.dtos.response.usecase.CountryPopulationData;
import com.klasha.demographicapp.dtos.response.usecase.CountryStateData;
import com.klasha.demographicapp.service.DemographicService;
import com.klasha.demographicapp.service.ResourceService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

/**
 * It is optimal to run long executing tasks
 * on different threads especially third-party
 * api calls
 */


@Slf4j
@Service
@RequiredArgsConstructor
public class DemographicServiceImpl implements DemographicService {

  private final ResourceService resourceService;

  @Override
  public List<MostPopulatedData> getMostPopulatedCities(int numberOfCities) {

    //This can be optimized later on
    int fixedNumberOfCitiesPerCountry = 10;

    List<CountryPopulationData> results = new ArrayList<>();
    List<CompletableFuture<CountryPopulationData>> futures = new ArrayList<>();

    for (String country : getCustomCountries()) {
      CompletableFuture<CountryPopulationData> future = resourceService
          .getResource("/api/v0.1/countries/population/cities/filter/q",
              Map.of(
              "limit", String.valueOf(fixedNumberOfCitiesPerCountry),
              "order", "asc",
              "orderBy", "name",
              "country", country
          ), CountryPopulationData.class,null);
      futures.add(future);
    }

    for (CompletableFuture<CountryPopulationData> future : futures) {
      try {
        CountryPopulationData countryPopulationData = future.get();
        results.add(countryPopulationData);
      } catch (InterruptedException | ExecutionException | HttpClientErrorException e) {
        log.info("::: "+e+" :::");
      }
    }

    if(!results.isEmpty()) {
      List<CountryPopulationData.PopulationData> populationData = results.stream()
          .flatMap(cityData -> cityData.getData().stream())
          .sorted(Collections.reverseOrder())
          .limit(numberOfCities)
          .collect(Collectors.toList());

      return populationData.stream()
          .map(p->new MostPopulatedData(
              p.getCity(),
              p.getLatestPopulationCount().getValue()))
          .collect(Collectors.toList());
    }
    return null;
  }

  @Override
  public CountryData getCountryInfo(String country) {

    CountryData countryData = new CountryData();

    CompletableFuture<CountryCapitalData> capitalCompletableFuture = resourceService
        .getResource(
            "/api/v0.1/countries/capital/q",
            Map.of("country", country),
            CountryCapitalData.class,null
        );

    CompletableFuture<CountryCurrencyData> currencyCompletableFuture = resourceService
        .getResource(
            "/api/v0.1/countries/currency/q",
            Map.of("country", country),
            CountryCurrencyData.class,null
        );

    CompletableFuture<CountryLocationData> locationCompletableFuture = resourceService
        .getResource(
            "/api/v0.1/countries/positions/q",
            Map.of("country", country),
            CountryLocationData.class,null
        );

    try {
      CountryCapitalData countryCapitalData = capitalCompletableFuture.get(TIME_OUT, TimeUnit.MILLISECONDS);

      if(Objects.nonNull(countryCapitalData) && !countryCapitalData.getError() && Objects.nonNull(countryCapitalData.getData())){

        CountryCapitalData.CapitalData capitalData = countryCapitalData.getData();
        countryData.setCapital(capitalData.getCapital());
        countryData.setIso2(capitalData.getIso2());
        countryData.setIso3(capitalData.getIso3());

        CompletableFuture<CityPopulationData> populationCompletableFuture = resourceService
            .getResource(
                "/api/v0.1/countries/population/cities/q",
                Map.of("city", countryData.getCapital()),
                CityPopulationData.class,null
            );

        CityPopulationData cityPopulationData = populationCompletableFuture.get(TIME_OUT,TimeUnit.MILLISECONDS);

        if(Objects.nonNull(cityPopulationData) && !cityPopulationData.getError() &&
            Objects.nonNull(cityPopulationData.getData().getLatestPopulationCount())){
          PopulationCount populationCount = cityPopulationData.getData().getLatestPopulationCount();
          countryData.setPopulation(populationCount.getValue());
        }

        CountryCurrencyData currencyData = currencyCompletableFuture.get(TIME_OUT, TimeUnit.MILLISECONDS);
        if(Objects.nonNull(currencyData) && !currencyData.getError() && Objects.nonNull(currencyData.getData())){
          countryData.setCurrency(currencyData.getData().getCurrency());
        }

        CountryLocationData locationData = locationCompletableFuture.get(TIME_OUT, TimeUnit.MILLISECONDS);
        if(!locationData.getError() && Objects.nonNull(locationData.getData())){
          countryData.setLocation(new LocationData(
              locationData.getData().getLongitude(),
              locationData.getData().getLatitude()));
        }

      }

      return countryData;
    } catch (Exception e) {
      log.error("::: "+e+" :::");
      return null;
    }
  }

  @Override
  public List<StateData> getCitiesInState(String country) {
    List<StateData> listOfStateData = new ArrayList<>();

    CompletableFuture<CountryStateData> statesCompletableFuture = resourceService
        .getResource(
            "/api/v0.1/countries/states/q",
            Map.of("country", country),
            CountryStateData.class,null
        );

    try {
      CountryStateData countryStateData = statesCompletableFuture
          .get(TIME_OUT, TimeUnit.MILLISECONDS);

      if(Objects.nonNull(countryStateData) && !countryStateData.getError()
          && !isNullOrEmpty(countryStateData.getData().getStates())){

        countryStateData.getData().getStates()
            .forEach(s->{
              StateData stateData = new StateData();
              stateData.setName(s.getName());

              String stateSearchTerm = stateData.getName().replace(" State","");

              //the endpoint to get cities by state and country does not work
              //Fixes should be made from the third party endpoint
              listOfStateData.add(stateData);
            });
      }
      return listOfStateData;
    } catch (Exception e) {
      log.error("::: "+e+" :::");
      return null;
    }
  }

  private List<String> getCustomCountries(){
    return List.of("ghana","New Zealand","italy");
  }

}
