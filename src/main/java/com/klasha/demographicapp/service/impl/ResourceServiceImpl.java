package com.klasha.demographicapp.service.impl;

import static com.klasha.demographicapp.core.config.Urls.BASE_URL;
import static com.klasha.demographicapp.core.config.AsyncConfiguration.TASK_EXECUTOR_SERVICE;
import static com.klasha.demographicapp.core.utils.StringUtils.removeUrlEncodedSpaces;

import com.klasha.demographicapp.core.proxy.ProxyRequest;
import com.klasha.demographicapp.core.proxy.ProxyService;
import com.klasha.demographicapp.service.ResourceService;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * This class provides a generic method to receive
 * any get request asynchronously
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class ResourceServiceImpl implements ResourceService {

  private final ProxyService proxyService;

  @Async(TASK_EXECUTOR_SERVICE)
  @Override
  public <R,P> CompletableFuture<P> getResource(String url,
      Map<String, String> params,
      Class<P> responseClass,
      Class<R> requestClass) {

    UriComponentsBuilder builder = UriComponentsBuilder
        .fromUriString(BASE_URL+url);
    params.forEach(builder::queryParam);

    ProxyRequest proxyRequest = ProxyRequest.builder()
        .url(removeUrlEncodedSpaces(builder.toUriString()))
        .httpMethod(HttpMethod.GET)
        .build();

    CompletableFuture<P> completableFuture
        = proxyService.execute(proxyRequest, responseClass,requestClass);

    try {
      return completableFuture;
    } catch (HttpClientErrorException e) {
      log.error("::: "+e.getMessage()+" :::");
      return null;
    }
  }
}
