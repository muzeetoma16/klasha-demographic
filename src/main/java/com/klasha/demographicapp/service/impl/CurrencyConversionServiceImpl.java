package com.klasha.demographicapp.service.impl;

import static com.klasha.demographicapp.core.currency.CurrencyExchangeRates.currencyExchange;
import static com.klasha.demographicapp.core.proxy.ProxyService.TIME_OUT;

import com.klasha.demographicapp.core.currency.Currency;
import com.klasha.demographicapp.core.exception.BadRequestException;
import com.klasha.demographicapp.dtos.request.CurrencyConversionRequest;
import com.klasha.demographicapp.dtos.response.CurrencyConversionData;
import com.klasha.demographicapp.dtos.response.usecase.CountryCurrencyData;
import com.klasha.demographicapp.service.CurrencyConversionService;
import com.klasha.demographicapp.service.ResourceService;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CurrencyConversionServiceImpl implements CurrencyConversionService {

  private final ResourceService resourceService;

  @Override
  public CurrencyConversionData convertCurrency(
      CurrencyConversionRequest currencyConversionRequest) {

    CompletableFuture<CountryCurrencyData> currencyCompletableFuture = resourceService
        .getResource(
            "/api/v0.1/countries/currency/q",
            Map.of("country", currencyConversionRequest.getCountry()),
            CountryCurrencyData.class,null
        );

    try {
      CountryCurrencyData currencyData = currencyCompletableFuture.get(TIME_OUT, TimeUnit.MILLISECONDS);
      if(Objects.nonNull(currencyData) && !currencyData.getError()
          && Objects.nonNull(currencyData.getData())){

        String currency = currencyData.getData().getCurrency();

        Currency countryCurrency = Currency.valueOf(currency);
        Currency targetCurrency = Currency.valueOf(currencyConversionRequest.getTargetCurrency());

        BigDecimal convertedAmount =  currencyExchange(
            countryCurrency,
            targetCurrency,
            currencyConversionRequest.getAmount()
        );

        String convertedCountryAmountWithCurrency = countryCurrency.name()+" "+currencyConversionRequest.getAmount();
        String convertedTargetAmountWithCurrency = targetCurrency.name() +" "+ convertedAmount;

        return new CurrencyConversionData(convertedCountryAmountWithCurrency,convertedTargetAmountWithCurrency);
      }
      throw new BadRequestException("Could retrieve country info by "+currencyConversionRequest.getCountry());

    }catch (IllegalArgumentException e){
      log.error("::: "+e.getMessage());
      throw new BadRequestException("Could not convert currency. Invalid currency "+ currencyConversionRequest.getTargetCurrency()+"passed ");
    } catch (ExecutionException | InterruptedException | TimeoutException e) {
      log.error("::: "+e.getMessage());
      throw new BadRequestException("Could retrieve country info");
    }
  }
}
