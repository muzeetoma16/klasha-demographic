package com.klasha.demographicapp.service;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

public interface ResourceService {
  <R,P> CompletableFuture<P> getResource(String url, Map<String,String> params,Class<P> responseClass, Class<R> requestClass);
}
