package com.klasha.demographicapp.service;

import com.klasha.demographicapp.dtos.request.CurrencyConversionRequest;
import com.klasha.demographicapp.dtos.response.CurrencyConversionData;

public interface CurrencyConversionService {
  CurrencyConversionData convertCurrency(CurrencyConversionRequest currencyConversionRequest);
}
