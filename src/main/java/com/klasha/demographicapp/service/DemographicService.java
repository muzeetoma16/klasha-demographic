package com.klasha.demographicapp.service;

import com.klasha.demographicapp.dtos.response.CountryData;
import com.klasha.demographicapp.dtos.response.MostPopulatedData;
import com.klasha.demographicapp.dtos.response.StateData;
import java.util.List;

public interface DemographicService {

  List<MostPopulatedData> getMostPopulatedCities(int N);

  CountryData getCountryInfo(String country);

  List<StateData> getCitiesInState(String country);

}
