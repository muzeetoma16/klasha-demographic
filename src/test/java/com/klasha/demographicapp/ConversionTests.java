package com.klasha.demographicapp;

import static com.klasha.demographicapp.core.currency.CurrencyExchangeRates.currencyExchange;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.klasha.demographicapp.core.currency.Currency;
import java.math.BigDecimal;
import java.math.RoundingMode;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ConversionTests {

  @Test
  void accurateCurrencyReturnedWhenConverted(){

    BigDecimal amountToConvert = BigDecimal.valueOf(5000);
    Currency countryCurrency = Currency.NGN;
    Currency targetCurrency = Currency.EUR;

    BigDecimal expectedOutCome = BigDecimal.valueOf(10.14);

    BigDecimal convertedAmount =  currencyExchange(
        countryCurrency,
        targetCurrency,
        amountToConvert
    );

    assertEquals(convertedAmount,expectedOutCome);
  }

  @Test
  void accurateCurrencyRemainsSameWhenNotExchangeNotFound(){

    BigDecimal amountToConvert = BigDecimal.valueOf(5000);
    Currency countryCurrency = Currency.EUR;
    Currency targetCurrency = Currency.USD;

    BigDecimal expectedOutCome = BigDecimal.valueOf(5000)
        .setScale(2, RoundingMode.HALF_UP);

    BigDecimal convertedAmount =  currencyExchange(
        countryCurrency,
        targetCurrency,
        amountToConvert
    );

    assertEquals(expectedOutCome,convertedAmount);
  }

}
