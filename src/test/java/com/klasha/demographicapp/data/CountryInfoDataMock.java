package com.klasha.demographicapp.data;

import com.klasha.demographicapp.core.currency.Currency;
import com.klasha.demographicapp.dtos.response.usecase.CityPopulationData;
import com.klasha.demographicapp.dtos.response.usecase.CityPopulationData.PopulationData;
import com.klasha.demographicapp.dtos.response.usecase.CityPopulationData.PopulationData.PopulationCount;
import com.klasha.demographicapp.dtos.response.usecase.CountryCapitalData;
import com.klasha.demographicapp.dtos.response.usecase.CountryCapitalData.CapitalData;
import com.klasha.demographicapp.dtos.response.usecase.CountryCurrencyData;
import com.klasha.demographicapp.dtos.response.usecase.CountryCurrencyData.CurrencyData;
import com.klasha.demographicapp.dtos.response.usecase.CountryLocationData;
import com.klasha.demographicapp.dtos.response.usecase.CountryLocationData.LocationData;
import com.klasha.demographicapp.dtos.response.usecase.CountryStateData;
import java.util.ArrayList;
import java.util.List;

public class CountryInfoDataMock {

  public static CountryCapitalData getCapitalData(){
    CountryCapitalData data = new CountryCapitalData();
    CapitalData capitalData = new CapitalData();

    capitalData.setName("United States");
    capitalData.setCapital("Washington, D.C.");
    capitalData.setIso2("US");
    capitalData.setIso3("USA");
    data.setData(capitalData);
    data.setError(false);
    return data;
  }

  public static CountryCurrencyData getCurrencyData(){
    CountryCurrencyData data = new CountryCurrencyData();
    CurrencyData currencyData = new CurrencyData();

    currencyData.setName("United States");
    currencyData.setCurrency("USD");
    data.setData(currencyData);
    data.setError(false);
    return data;
  }

  public static CountryLocationData getLocationData(){
    CountryLocationData data = new CountryLocationData();
    LocationData locationData = new LocationData();

    locationData.setName("New York");
    locationData.setLongitude(-74L);
    locationData.setLatitude(-40L);
    data.setError(false);
    data.setData(locationData);
    return data;
  }

  public static CityPopulationData getCityPopulationData(){
    CityPopulationData cityPopulationData = new CityPopulationData();
    PopulationData populationData = new PopulationData();
    List<PopulationCount> populationCounts = new ArrayList<>();

    PopulationData.PopulationCount populationCount1 = new PopulationData.PopulationCount();
    populationCount1.setYear("2022");
    populationCount1.setValue("100000");
    populationCount1.setSex("Male");
    populationCount1.setReliability("High");

    PopulationData.PopulationCount populationCount2 = new PopulationData.PopulationCount();
    populationCount2.setYear("2022");
    populationCount2.setValue("95000");
    populationCount2.setSex("Female");
    populationCount2.setReliability("High");

    populationCounts.add(populationCount1);
    populationCounts.add(populationCount2);

    populationData.setCity("New York");
    populationData.setCountry("United States");
    populationData.setPopulationCounts(populationCounts);
    cityPopulationData.setError(false);
    cityPopulationData.setMsg("");
    cityPopulationData.setData(populationData);

    return cityPopulationData;
  }

  public static CountryStateData getStateData(){

    CountryStateData countryStateData = new CountryStateData();
    CountryStateData.StateData stateData = new CountryStateData.StateData();
    List<CountryStateData.State> states = new ArrayList<>();

    CountryStateData.State state1 = new CountryStateData.State();
    state1.setName("New York");
    state1.setStateCode("NY");

    CountryStateData.State state2 = new CountryStateData.State();
    state2.setName("California");
    state2.setStateCode("CA");
    states.add(state1);
    states.add(state2);
    stateData.setStates(states);
    countryStateData.setData(stateData);
    countryStateData.setError(false);

    return countryStateData;
  }
}
