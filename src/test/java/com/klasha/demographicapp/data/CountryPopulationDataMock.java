package com.klasha.demographicapp.data;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.klasha.demographicapp.dtos.response.usecase.CountryPopulationData;
import com.klasha.demographicapp.dtos.response.usecase.CountryPopulationData.PopulationData;
import com.klasha.demographicapp.dtos.response.usecase.CountryPopulationData.PopulationData.PopulationCount;
import java.util.Arrays;
import java.util.List;

public class CountryPopulationDataMock {

  public static List<CountryPopulationData> getCountryPopulationDataList() {
    CountryPopulationData data1 = createMockCountryPopulationData("City1", "Country1", "2022", "100000");
    CountryPopulationData data2 = createMockCountryPopulationData("City2", "Country2", "2023", "150000");
    CountryPopulationData data3 = createMockCountryPopulationData("City3", "Country3", "2020", "120000");
    return Arrays.asList(data1, data2, data3);
  }

  private static CountryPopulationData createMockCountryPopulationData(
      String city, String country, String year, String value) {
    PopulationData populationData = createMockPopulationData(city, country, year, value);
    return new CountryPopulationData(List.of(populationData));
  }

  private static PopulationData createMockPopulationData(
      String city, String country, String year, String value) {

    PopulationData populationData = new PopulationData();
    populationData.setCity(city);
    populationData.setCountry(country);

    PopulationCount populationCount = new PopulationCount();
    populationCount.setYear(year);
    populationCount.setValue(value);

    populationData.setPopulationCounts(List.of(populationCount));
    return populationData;
  }
}

