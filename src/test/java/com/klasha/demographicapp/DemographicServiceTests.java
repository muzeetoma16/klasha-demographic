package com.klasha.demographicapp;

import static com.klasha.demographicapp.data.CountryInfoDataMock.getCapitalData;
import static com.klasha.demographicapp.data.CountryInfoDataMock.getCityPopulationData;
import static com.klasha.demographicapp.data.CountryInfoDataMock.getCurrencyData;
import static com.klasha.demographicapp.data.CountryInfoDataMock.getLocationData;
import static com.klasha.demographicapp.data.CountryInfoDataMock.getStateData;
import static com.klasha.demographicapp.data.CountryPopulationDataMock.getCountryPopulationDataList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.klasha.demographicapp.dtos.response.CountryData;
import com.klasha.demographicapp.dtos.response.MostPopulatedData;
import com.klasha.demographicapp.dtos.response.StateData;
import com.klasha.demographicapp.dtos.response.usecase.CityPopulationData;
import com.klasha.demographicapp.dtos.response.usecase.CountryCapitalData;
import com.klasha.demographicapp.dtos.response.usecase.CountryCurrencyData;
import com.klasha.demographicapp.dtos.response.usecase.CountryLocationData;
import com.klasha.demographicapp.dtos.response.usecase.CountryPopulationData;
import com.klasha.demographicapp.dtos.response.usecase.CountryStateData;
import com.klasha.demographicapp.service.DemographicService;
import com.klasha.demographicapp.service.ResourceService;
import com.klasha.demographicapp.service.impl.DemographicServiceImpl;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.junit.jupiter.api.Test;


class DemographicServiceTests {

	private final ResourceService resourceService =mock(ResourceService.class);

  @Test
	void testGetMostPopulatedCities() {
		DemographicService demographicService = new DemographicServiceImpl(resourceService);

		when(resourceService.getResource(anyString(), anyMap(), eq(CountryPopulationData.class),any()))
				.thenReturn(CompletableFuture.completedFuture(getCountryPopulationDataList().get(0)));

		int[] N = new int[]{0,1,2,3};

		//The number of cities returned must be the limit or less
		for (int n:N) {
			List<MostPopulatedData> result = demographicService.getMostPopulatedCities(n);
			assertTrue(result.size()<=n);
		}
	}

	@Test
	void testCountryInfo(){
		when(resourceService.getResource(anyString(), anyMap(), eq(CountryCapitalData.class),any()))
				.thenReturn(CompletableFuture.completedFuture(getCapitalData()));

		when(resourceService.getResource(anyString(), anyMap(), eq(CountryCurrencyData.class),any()))
				.thenReturn(CompletableFuture.completedFuture(getCurrencyData()));

		when(resourceService.getResource(anyString(), anyMap(), eq(CountryLocationData.class),any()))
				.thenReturn(CompletableFuture.completedFuture(getLocationData()));

		when(resourceService.getResource(anyString(), anyMap(), eq(CityPopulationData.class),any()))
				.thenReturn(CompletableFuture.completedFuture(getCityPopulationData()));

		DemographicService demographicService = new DemographicServiceImpl(resourceService);

		String country = "any";

		CountryData data = demographicService.getCountryInfo(country);

		assertNotNull(data);
		assertNotEquals("", data.getCapital());
	}

	@Test
	void testStateInfo(){
		when(resourceService.getResource(anyString(), anyMap(), eq(CountryStateData.class),any()))
				.thenReturn(CompletableFuture.completedFuture(getStateData()));

		DemographicService demographicService = new DemographicServiceImpl(resourceService);

		String country = "any";

		List<StateData> data = demographicService.getCitiesInState(country);

		assertNotNull(data);
		assertNotEquals(Collections.emptyList(), data);
	}


}
